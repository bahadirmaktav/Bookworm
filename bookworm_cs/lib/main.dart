import 'package:bookworm_cs/core/utils/applocalization.dart';
import 'package:bookworm_cs/features/bookworm/presentation/bloc/auth_bloc/auth_bloc_bloc.dart';
import 'package:bookworm_cs/features/bookworm/presentation/pages/login_page.dart';
import 'package:bookworm_cs/features/bookworm/presentation/pages/register_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'injection_container.dart' as di;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      supportedLocales: [
        Locale('en', 'US'),
        Locale('tr', 'TR'),
      ],
      // These delegates make sure that the localization data for the proper language is loaded
      localizationsDelegates: [
        // THIS CLASS WILL BE ADDED LATER
        // A class which loads the translations from JSON files
        AppLocalizations.delegate,
        // Built-in localization of basic text for Material widgets
        GlobalMaterialLocalizations.delegate,
        // Built-in localization for text direction LTR/RTL
        GlobalWidgetsLocalizations.delegate,
      ],
      // Returns a locale which will be used by the app
      localeResolutionCallback: (locale, supportedLocales) {
        // Check if the current device locale is supported
        for (var supportedLocale in supportedLocales) {
          if (supportedLocale.languageCode == locale.languageCode &&
              supportedLocale.countryCode == locale.countryCode) {
            return supportedLocale;
          }
        }
        // If the locale of the device is not supported, use the first one
        // from the list (English, in this case).
        return supportedLocales.first;
      },
      theme: ThemeData(
        scaffoldBackgroundColor: Color.fromRGBO(66, 92, 90, 1),
        accentColor: Color.fromRGBO(66, 92, 90, 1),
        buttonTheme: ButtonThemeData(
          buttonColor: Color.fromRGBO(255, 206, 162, 1),
          textTheme: ButtonTextTheme.accent,
        ),
        primaryColor: Color.fromRGBO(255, 206, 162, 1),
        hintColor: Colors.white,
        errorColor: Color.fromRGBO(255, 206, 162, 1),
        appBarTheme: AppBarTheme(
            iconTheme: IconThemeData(color: Color.fromRGBO(255, 206, 162, 1))),
        inputDecorationTheme: InputDecorationTheme(
            enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.white))),
        fontFamily: 'Georgia',
        textTheme: TextTheme(
          subhead: TextStyle(color: Colors.white),
        ),
      ),
      home: BlocProvider<AuthBlocBloc>(
        create: (BuildContext context) => AuthBlocBloc(di.sl()),
        child: LoginPage(),
      ),
    );
  }
}
