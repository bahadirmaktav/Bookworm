class UserModel {
  final String email;
  final String password;
  final String username;
  final String name;
  final String token;

  UserModel({
    this.email,
    this.password,
    this.username,
    this.name,
    this.token,
  });

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      email: json['email'],
      username: json['username'],
      name: json['name'],
      token: json['token'],
    );
  }

  Map<String, dynamic> toJson(UserModel userModel) {
    return {
      'email': userModel.email,
      'password': userModel.password,
      'username': userModel.username,
      'name': userModel.name
    };
  }
}
