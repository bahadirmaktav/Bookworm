import 'dart:convert';
import 'package:bookworm_cs/core/error/exceptions.dart';
import 'package:meta/meta.dart';
import 'package:bookworm_cs/core/utils/network.dart';
import 'package:bookworm_cs/core/utils/urls.dart';
import 'package:bookworm_cs/features/bookworm/data/models/user_model.dart';
import 'package:http/http.dart' as http;

abstract class UserAuthDS {
  Future<String> register({@required UserModel newUser});
  Future<UserModel> signIn(
      {@required String password, @required String username});
}

class UserAuthDSIMPL implements UserAuthDS {
  final NetworkInfo networkInfo;
  final http.Client client;

  UserAuthDSIMPL(this.networkInfo, this.client);

  @override
  Future<String> register({UserModel newUser}) async {
    if (await networkInfo.isConnected) {
      final response = await client.post(
        REGISTER_URL,
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode(newUser.toJson(newUser)),
      );
      if (response.statusCode == 200) {
        return jsonDecode(response.body)['username'];
      } else {
        switch (jsonDecode(response.body)['errorMessage']) {
          case "AUTHENTICATION_ERROR":
            throw AuthenticationException();
            break;
          case "ERROR_USERNAME_ALREADY_IN_USE":
            throw AlreadyExistEmailInputException();
            break;
          default:
            throw ServerException();
        }
      }
    } else {
      throw ConnectionException();
    }
  }

  @override
  Future<UserModel> signIn({String password, String username}) async {
    if (await networkInfo.isConnected) {
      final response = await client.post(
        SIGNIN_URL,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json",
        },
        body: jsonEncode(
            <String, dynamic>{'password': password, 'username': username}),
      );
      if (response.statusCode == 200) {
        return UserModel.fromJson(jsonDecode(response.body));
      } else {
        switch (jsonDecode(response.body)['errorMessage']) {
          case "AUTHENTICATION_ERROR":
            throw AuthenticationException();
            break;
          case "ERROR_WRONG_USERNAME":
            throw WrongUsernameInputException();
            break;
          case "ERROR_WRONG_PASSWORD":
            throw WrongPasswordInputException();
            break;
          default:
            throw ServerException();
        }
      }
    } else {
      throw ConnectionException();
    }
  }
}
