import 'package:flutter/material.dart';

class SnackBarTextWidget extends StatelessWidget {
  final String message;

  const SnackBarTextWidget({Key key, this.message}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Text(
      message,
      style: TextStyle(fontSize: 15,),
    );
  }
}
