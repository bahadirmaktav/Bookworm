import 'package:flutter/material.dart';

class LoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height / 10,
      child: Center(
        child: CircularProgressIndicator(
          backgroundColor: Color.fromRGBO(255, 206, 162, 1),
        ),
      ),
    );
  }
}
