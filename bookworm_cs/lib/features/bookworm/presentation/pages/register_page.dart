import 'package:bookworm_cs/core/utils/applocalization.dart';
import 'package:bookworm_cs/core/utils/messages_constants.dart';
import 'package:bookworm_cs/features/bookworm/data/models/user_model.dart';
import 'package:bookworm_cs/features/bookworm/presentation/bloc/auth_bloc/auth_bloc_bloc.dart';
import 'package:bookworm_cs/features/bookworm/presentation/widgets/loading_widget.dart';
import 'package:bookworm_cs/features/bookworm/presentation/widgets/snackbar_text_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final validCharacters = RegExp(r'^[a-zA-Z0-9]+$');
  final _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String _email;
  String _password;
  String _name;
  String _username;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Form(
          key: _formKey,
          child: Center(
            child: Column(
              children: <Widget>[
                SizedBox(height: 80),
                Container(
                    height: MediaQuery.of(context).size.height / 8.5,
                    width: MediaQuery.of(context).size.height / 2.1,
                    child: TextFormField(
                      onSaved: (value) => _name = value,
                      autovalidate: _autoValidate,
                      validator: (value) {
                        if (value.length < 1) {
                          return AppLocalizations.of(context)
                              .translate(EMPTY_NAME_INPUT_FAILURE_MESSAGE);
                        } else {
                          return null;
                        }
                      },
                      decoration: InputDecoration(
                          labelText: AppLocalizations.of(context)
                              .translate(ENTER_NAME_LABEL),
                          hintText: AppLocalizations.of(context)
                              .translate(ENTER_NAME),
                          prefixIcon: Icon(Icons.person,
                              color: Color.fromRGBO(255, 206, 162, 1))),
                    )),
                SizedBox(height: 20),
                Container(
                    height: MediaQuery.of(context).size.height / 8.5,
                    width: MediaQuery.of(context).size.height / 2.1,
                    child: TextFormField(
                      onSaved: (value) => _email = value,
                      autovalidate: _autoValidate,
                      validator: (value) {
                        if (value.length < 1) {
                          return AppLocalizations.of(context)
                              .translate(EMPTY_EMAIL_INPUT_FAILURE_MESSAGE);
                        } else {
                          return null;
                        }
                      },
                      decoration: InputDecoration(
                          labelText: AppLocalizations.of(context)
                              .translate(ENTER_EMAIL_LABEL),
                          hintText: AppLocalizations.of(context)
                              .translate(ENTER_EMAIL),
                          prefixIcon: Icon(Icons.email,
                              color: Color.fromRGBO(255, 206, 162, 1))),
                    )),
                SizedBox(height: 20),
                Container(
                    height: MediaQuery.of(context).size.height / 8.5,
                    width: MediaQuery.of(context).size.height / 2.1,
                    child: TextFormField(
                      onSaved: (value) => _password = value,
                      autovalidate: _autoValidate,
                      obscureText: true,
                      validator: (value) {
                        if (!validCharacters.hasMatch(value)) {
                          return AppLocalizations.of(context).translate(
                              PASSWORD_CONTAINS_SPECIAL_CHARS_FAILURE_MESSAGE);
                        } else if (value.length <= 6) {
                          return AppLocalizations.of(context)
                              .translate(LENGHT_INPUT_FAILURE_MESSAGE);
                        } else {
                          return null;
                        }
                      },
                      decoration: InputDecoration(
                          labelText: AppLocalizations.of(context)
                              .translate(ENTER_PASSWORD_LABEL),
                          hintText: AppLocalizations.of(context)
                              .translate(ENTER_PASSWORD),
                          prefixIcon: Icon(Icons.lock,
                              color: Color.fromRGBO(255, 206, 162, 1))),
                    )),
                SizedBox(height: 20),
                Container(
                    height: MediaQuery.of(context).size.height / 8.5,
                    width: MediaQuery.of(context).size.height / 2.1,
                    child: TextFormField(
                      onSaved: (value) => _username = value,
                      autovalidate: _autoValidate,
                      validator: (value) {
                        if (value.length < 1) {
                          return AppLocalizations.of(context)
                              .translate(EMPTY_USERNAME_INPUT_FAILURE_MESSAGE);
                        } else {
                          return null;
                        }
                      },
                      decoration: InputDecoration(
                          labelText: AppLocalizations.of(context)
                              .translate(ENTER_USERNAME_LABEL),
                          hintText: AppLocalizations.of(context)
                              .translate(ENTER_USERNAME),
                          prefixIcon: Icon(Icons.person,
                              color: Color.fromRGBO(255, 206, 162, 1))),
                    )),
                SizedBox(height: 40),
                Container(
                  height: MediaQuery.of(context).size.height / 16,
                  width: MediaQuery.of(context).size.height / 3.5,
                  child: RaisedButton(
                    onPressed: () {
                      if (_validateInputs()) {
                        BlocProvider.of<AuthBlocBloc>(context)
                            .add(RegisterEvent(
                                newUser: UserModel(
                          email: _email,
                          password: _password,
                          username: _username,
                          name: _name,
                        )));
                        _formKey.currentState.reset();
                      }
                    },
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0)),
                    child: Text(
                      AppLocalizations.of(context).translate(REGISTER),
                      style: TextStyle(fontSize: 12),
                    ),
                  ),
                ),
                SizedBox(height: 35),
                BlocListener<AuthBlocBloc, AuthBlocState>(
                  listener: (context, state) {
                    if (state is RegisteredState) {
                      Scaffold.of(context).showSnackBar(SnackBar(
                          content: SnackBarTextWidget(
                              message: AppLocalizations.of(context)
                                  .translate(state.message))));
                    } else if (state is AuthErrorState) {
                      Scaffold.of(context).showSnackBar(SnackBar(
                          content: SnackBarTextWidget(
                              message: AppLocalizations.of(context)
                                  .translate(state.errorMessage))));
                    }
                  },
                  child: BlocBuilder<AuthBlocBloc, AuthBlocState>(
                    builder: (context, state) {
                      if (state is AuthLoadingState) {
                        return LoadingWidget();
                      } else
                        return Text('');
                    },
                  ),
                )
              ],
            ),
          )),
    );
  }

  bool _validateInputs() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      return true;
    } else {
      setState(() {
        _autoValidate = true;
      });
      return false;
    }
  }
}
