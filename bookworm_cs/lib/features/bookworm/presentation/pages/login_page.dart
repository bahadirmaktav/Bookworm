import 'package:bookworm_cs/core/utils/applocalization.dart';
import 'package:bookworm_cs/core/utils/messages_constants.dart';
import 'package:bookworm_cs/features/bookworm/presentation/bloc/auth_bloc/auth_bloc_bloc.dart';
import 'package:bookworm_cs/features/bookworm/presentation/pages/home_page.dart';
import 'package:bookworm_cs/features/bookworm/presentation/pages/register_page.dart';
import 'package:bookworm_cs/features/bookworm/presentation/widgets/loading_widget.dart';
import 'package:bookworm_cs/features/bookworm/presentation/widgets/snackbar_text_widget.dart';
import 'package:bookworm_cs/injection_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String _username;
  String _password;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Form(
          key: _formKey,
          child: Center(
            child: Column(
              children: <Widget>[
                SizedBox(height: 90),
                Container(
                  height: MediaQuery.of(context).size.height / 15,
                  width: MediaQuery.of(context).size.height / 3,
                  child: Image.asset(
                    'images/logo.png',
                    color: Color.fromRGBO(255, 206, 162, 1),
                  ),
                ),
                SizedBox(height: 55),
                Container(
                    height: MediaQuery.of(context).size.height / 8,
                    width: MediaQuery.of(context).size.height / 2,
                    child: TextFormField(
                      onSaved: (value) => _username = value,
                      autovalidate: _autoValidate,
                      validator: (value) {
                        if (value.length < 1) {
                          return AppLocalizations.of(context)
                              .translate(EMPTY_USERNAME_INPUT_FAILURE_MESSAGE);
                        } else {
                          return null;
                        }
                      },
                      decoration: InputDecoration(
                        labelText: AppLocalizations.of(context)
                            .translate(ENTER_USERNAME_LABEL),
                        hintText: AppLocalizations.of(context)
                            .translate(ENTER_USERNAME),
                        prefixIcon: Icon(Icons.person,
                            color: Color.fromRGBO(255, 206, 162, 1)),
                      ),
                    )),
                SizedBox(height: 30),
                Container(
                    height: MediaQuery.of(context).size.height / 8,
                    width: MediaQuery.of(context).size.height / 2,
                    child: TextFormField(
                      onSaved: (value) => _password = value,
                      autovalidate: _autoValidate,
                      obscureText: true,
                      validator: (value) {
                        if (value.length < 1) {
                          return AppLocalizations.of(context)
                              .translate(EMPTY_PASSWORD_INPUT_FAILURE_MESSAGE);
                        } else {
                          return null;
                        }
                      },
                      decoration: InputDecoration(
                        labelText: AppLocalizations.of(context)
                            .translate(ENTER_PASSWORD_LABEL),
                        hintText: AppLocalizations.of(context)
                            .translate(ENTER_PASSWORD),
                        prefixIcon: Icon(Icons.lock,
                            color: Color.fromRGBO(255, 206, 162, 1)),
                      ),
                    )),
                SizedBox(height: 50),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height / 16,
                      width: MediaQuery.of(context).size.height / 4.4,
                      child: RaisedButton(
                        onPressed: () {
                          if (_validateInputs()) {
                            BlocProvider.of<AuthBlocBloc>(context).add(
                                SignInEvent(
                                    username: _username, password: _password));
                          }
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0)),
                        child: Text(
                          AppLocalizations.of(context).translate(LOGIN),
                          style: TextStyle(fontSize: 12),
                        ),
                      ),
                    ),
                    SizedBox(width: 30),
                    Container(
                      height: MediaQuery.of(context).size.height / 16,
                      width: MediaQuery.of(context).size.height / 4.4,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => BlocProvider<AuthBlocBloc>(
                              create: (BuildContext context) =>
                                  AuthBlocBloc(sl()),
                              child: RegisterPage(),
                            ),
                          ));
                        },
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0)),
                        child: Text(
                            AppLocalizations.of(context)
                                .translate(CREATE_ACCOUNT),
                            style: TextStyle(fontSize: 12)),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 30),
                BlocListener<AuthBlocBloc, AuthBlocState>(
                    listener: (BuildContext context, AuthBlocState state) {
                  if (state is SignedInState) {
                    Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => HomePage()),
                    );
                  } else if (state is AuthErrorState) {
                    Scaffold.of(context).showSnackBar(SnackBar(
                        content: SnackBarTextWidget(
                            message: AppLocalizations.of(context)
                                .translate(state.errorMessage))));
                  } else if (state is RegisteredState) {
                    Scaffold.of(context).showSnackBar(SnackBar(
                        content: SnackBarTextWidget(
                            message: AppLocalizations.of(context)
                                .translate(state.message))));
                  }
                }, child: BlocBuilder<AuthBlocBloc, AuthBlocState>(
                  builder: (context, state) {
                    if (state is AuthLoadingState) {
                      return LoadingWidget();
                    } else {
                      return Text('');
                    }
                  },
                ))
              ],
            ),
          ),
        ));
  }

  bool _validateInputs() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      return true;
    } else {
      setState(() {
        _autoValidate = true;
      });
      return false;
    }
  }
}
