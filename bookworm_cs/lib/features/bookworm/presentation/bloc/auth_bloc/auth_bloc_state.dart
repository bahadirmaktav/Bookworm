part of 'auth_bloc_bloc.dart';

@immutable
abstract class AuthBlocState {}

class AuthBlocInitial extends AuthBlocState {}

class SignedInState extends AuthBlocState {
  final UserModel user;

  SignedInState({@required this.user});
}

class RegisteredState extends AuthBlocState {
  final String username;
  final String message;

  RegisteredState({@required this.username, @required this.message});
}

class AuthErrorState extends AuthBlocState {
  final String errorMessage;

  AuthErrorState({@required this.errorMessage});
}

class AuthLoadingState extends AuthBlocState {}
