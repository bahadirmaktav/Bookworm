import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:bookworm_cs/core/error/exceptions.dart';
import 'package:bookworm_cs/core/utils/messages_constants.dart';
import 'package:bookworm_cs/features/bookworm/data/datasources/user_auth_ds.dart';
import 'package:bookworm_cs/features/bookworm/data/models/user_model.dart';
import 'package:meta/meta.dart';

part 'auth_bloc_event.dart';
part 'auth_bloc_state.dart';

class AuthBlocBloc extends Bloc<AuthBlocEvent, AuthBlocState> {
  final UserAuthDS userAuthDS;

  AuthBlocBloc(this.userAuthDS);

  @override
  AuthBlocState get initialState => AuthBlocInitial();

  @override
  Stream<AuthBlocState> mapEventToState(
    AuthBlocEvent event,
  ) async* {
    if (event is SignInEvent) {
      yield AuthLoadingState();
      try {
        final res = await userAuthDS.signIn(
            password: event.password, username: event.username);
        yield SignedInState(user: res);
      } catch (e) {
        yield AuthErrorState(errorMessage: _mapExceptionToMessage(e));
      }
    } else if (event is RegisterEvent) {
      try {
        yield AuthLoadingState();
        final res = await userAuthDS.register(newUser: event.newUser);
        yield RegisteredState(username: res, message: USER_REGISTERED_MESSAGE);
      } catch (e) {
        yield AuthErrorState(errorMessage: _mapExceptionToMessage(e));
      }
    }
  }
}

String _mapExceptionToMessage(Exception exception) {
  switch (exception.runtimeType) {
    case ServerException:
      return SERVER_FAILURE_MESSAGE;
    case ConnectionException:
      return CONNECTION_FAILURE_MESSAGE;
    case AuthenticationException:
      return AUTHENTICATION_FAILURE_MESSAGE;
    case AlreadyExistEmailInputException:
      return ALREADY_EXIST_EMAIL_FAILURE_MESSAGE;
    case WrongUsernameInputException:
      return WRONG_USERNAME_INPUT_FAILURE_MESSAGE;
    case WrongPasswordInputException:
      return WRONG_PASSWORD_INPUT_FAILURE_MESSAGE;
    default:
      return 'Unexpected Error';
  }
}
