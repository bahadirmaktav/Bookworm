part of 'auth_bloc_bloc.dart';

@immutable
abstract class AuthBlocEvent {}

class SignInEvent extends AuthBlocEvent {
  final String username;
  final String password;

  SignInEvent({
    @required this.username,
    @required this.password,
  });
}

class RegisterEvent extends AuthBlocEvent {
  final UserModel newUser;

  RegisterEvent({@required this.newUser});
}
