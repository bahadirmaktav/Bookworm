import 'package:bookworm_cs/core/utils/applocalization.dart';
import 'package:bookworm_cs/core/utils/network.dart';
import 'package:bookworm_cs/features/bookworm/data/datasources/user_auth_ds.dart';
import 'package:bookworm_cs/features/bookworm/presentation/bloc/auth_bloc/auth_bloc_bloc.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

final sl = GetIt.instance;

Future<void> init() async {
  //! Features - Number Trivia
  // Bloc
  sl.registerFactory(() => AuthBlocBloc(sl()));
  // Data sources
  sl.registerLazySingleton<UserAuthDS>(() => UserAuthDSIMPL(sl(), sl()));
  //! Core
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));
  sl.registerLazySingleton(() => AppLocalizations(sl()));
  //! External
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => DataConnectionChecker());
}
