//Server and Connection Failures
class ServerException implements Exception {}
class ConnectionException implements Exception {}

//Authentication and Validation Exceptions
class InvalidEmailInputException implements Exception{}
class AlreadyExistEmailInputException implements Exception{}
class WrongPasswordInputException implements Exception{}
class WrongUsernameInputException implements Exception{}
class UserNotFoundException implements Exception{}
class UserDisabledException implements Exception{}
class TooManyRequestsException implements Exception{}
class AuthenticationException implements Exception{}