//General Error Messages
const String SERVER_FAILURE_MESSAGE = 'server_failure_message';
const String CONNECTION_FAILURE_MESSAGE = 'connection_failure_message';

//Authentication Error Messages
const String LENGHT_INPUT_FAILURE_MESSAGE = 'lenght_input_failure_message';
const String INVALID_EMAIL_FAILURE_MESSAGE = 'invalid_email_failure_message';
const String ALREADY_EXIST_EMAIL_FAILURE_MESSAGE = 'already_exist_email_failure_message';
const String WRONG_PASSWORD_INPUT_FAILURE_MESSAGE = 'wrong_password_input_failure_message';
const String WRONG_USERNAME_INPUT_FAILURE_MESSAGE = 'wrong_username_input_failure_message';
const String USER_NOT_FOUND_FAILURE_MESSAGE = 'user_not_found_failure_message';
const String USER_DISABLED_FAILURE_MESSAGE = 'user_disabled_failure_message';
const String TOO_MANY_REQUESTS_FAILURE_MESSAGE = 'too_many_requests_failure_message';
const String AUTHENTICATION_FAILURE_MESSAGE = 'authentication_failure_message';
const String EMPTY_PASSWORD_INPUT_FAILURE_MESSAGE = 'empty_password_input_failure_message';
const String EMPTY_EMAIL_INPUT_FAILURE_MESSAGE = 'empty_email_input_failure_message';
const String EMPTY_NAME_INPUT_FAILURE_MESSAGE = 'empty_name_input_failure_message';
const String EMPTY_USERNAME_INPUT_FAILURE_MESSAGE = 'empty_username_input_failure_message';
const String PASSWORD_CONTAINS_SPECIAL_CHARS_FAILURE_MESSAGE = 'password_contains_special_chars_failure_message';

//Update Messages
const String UPDATE_MESSAGE = 'update_message';
const String USER_REGISTERED_MESSAGE = 'user_registered_message';
const String USER_SIGNEDIN_MESSAGE = 'user_signedin_message';

//UI Texts
  //Button Texts
const String LOGIN = 'login';
const String REGISTER = 'register';
const String CREATE_ACCOUNT = 'create_account';
  //Auth Form Texts
const String ENTER_EMAIL = 'enter_email';
const String ENTER_PASSWORD = 'enter_password';
const String ENTER_NAME = 'enter_name';
const String ENTER_USERNAME = 'enter_username';
const String ENTER_EMAIL_LABEL = 'enter_email_label';
const String ENTER_PASSWORD_LABEL = 'enter_password_label';
const String ENTER_NAME_LABEL = 'enter_name_label';
const String ENTER_USERNAME_LABEL = 'enter_username_label';
