const User = require('../models/user_model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const errorCodes = require('../error/error_codes');


exports.register = (req, res) => {
    if (!req.body) { res.status(400).json({ errorMessage: errorCodes.BAD_REQUEST }); }
    bcrypt.hash(req.body.password, 10, (err, hash) => {
        if (err) { res.status(401).json({ errorMessage: errorCodes.AUTHENTICATION_ERROR }); }
        else {
            req.body.password = hash;
            const newUser = new User({
                email: req.body.email,
                password: req.body.password,
                name: req.body.name,
                username: req.body.username,
                role: 'User',
                registration_date: new Date()
            });
            User.createNewUser(newUser, (err, data) => {
                if (err) { res.status(401).json({ errorMessage: errorCodes.ERROR_USERNAME_ALREADY_IN_USE }); }
                else { res.status(200).json({ ...data }); }
            });
        };
    });
};

exports.deleteAccount = (req, res) => {
    if (!req.body) { res.status(400).json({ errorMessage: errorCodes.BAD_REQUEST }); }
    User.deleteUser(req.body.user_id, (err, data) => {
        if (err) { res.status(401).json({ errorMessage: errorCodes.AUTHENTICATION_ERROR }); }
        else { res.status(200); }
    });
};

exports.signIn = (req, res) => {
    if (!req.body) { res.status(400).json({ errorMessage: errorCodes.BAD_REQUEST }); }
    User.checkUser(req.body.username, (err, data) => {
        if (err) { res.status(401).json({ errorMessage: errorCodes.AUTHENTICATION_ERROR }); }
        else if (!data[0]) { res.status(401).json({ errorMessage: errorCodes.ERROR_WRONG_USERNAME }); }
        else {
            bcrypt.compare(req.body.password, data[0].password, (err, result) => {
                if (err) { res.status(401).json({ errorMessage: errorCodes.AUTHENTICATION_ERROR }); }
                if (result) {
                    jwt.sign({ user_id: data[0].user_id, role: data[0].role }, process.env.JWT_KEY, { expiresIn: "1h" }, (err, token) => {
                        if (err) { res.status(401).json({ errorMessage: errorCodes.AUTHENTICATION_ERROR }); }
                        else { res.status(200).json({ token: token, name: data[0].name, email: data[0].email, username: data[0].username }); }
                    });
                }
                else { res.status(401).json({ errorMessage: errorCodes.ERROR_WRONG_PASSWORD }); }
            });
        }
    });
};
