const db = require('../config/db');
const bcrypt = require('bcrypt');

const User = function (user) {
    this.email = user.email;
    this.password = user.password;
    this.name = user.name;
    this.username = user.username;
    this.role = user.role;
    this.registration_date = user.registration_date;
};

User.createNewUser = (newUser, result) => {
    var sql = "INSERT INTO users (email, password, name, username, role, registration_date) VALUES (?,?,?,?,?,?)";
    var values = [newUser.email, newUser.password, newUser.name, newUser.username, newUser.role, newUser.registration_date];
    db.query(sql, values, (err, res) => {
        if (err) { result(err, null); return; }
        else { result(null, { username: newUser.username }); };
    });
};

User.deleteUser = (user_id, result) => {
    var sql = "DELETE FROM users WHERE user_id = ?";
    db.query(sql, user_id, (err, res) => {
        if (err) { result(err, null); return; }
        else { result(null, res) };
    });
};

User.checkUser = (username, result) => {
    var sql = "SELECT * FROM users WHERE username = ?";
    db.query(sql, username, (err, res) => {
        if (err) { result(err, null); return; }
        else { result(null, res) };
    });
};

module.exports = User;