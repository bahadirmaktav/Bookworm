const userController = require('../controllers/user_controller');
const express = require('express');
const router = express.Router();

router.post("/register", userController.register);

router.post("/signIn", userController.signIn);

router.post("/deleteAccount", userController.deleteAccount);

module.exports = router;